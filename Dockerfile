FROM ubuntu:18.04

RUN apt-get update \
	&& apt-get -y install \
		build-essential \
		cmake \
		git \
		libglew-dev \
		libsdl2-dev \
		libdevil-dev \
		libopenal-dev \
		libogg-dev \
		libvorbis-dev \
		libfreetype6-dev \
		p7zip-full \
		libxcursor-dev \
		libboost-thread-dev \
		libboost-regex-dev \
		libboost-system-dev \
		libboost-program-options-dev \
		libboost-signals-dev \
		libboost-chrono-dev \
		libboost-filesystem-dev \
		libunwind8-dev \
		default-jdk \
		libcurl4-gnutls-dev \
		coreutils \
		ccache

RUN /usr/sbin/update-ccache-symlinks
RUN ccache -M 5G
